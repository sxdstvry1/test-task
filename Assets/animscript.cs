using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Spine.Unity.Examples {
public class animscript : MonoBehaviour
{
    public AnimationReferenceAsset hoverboard;
    public AnimationReferenceAsset idle;
    SkeletonAnimation skeletonAnimation;
    public GameObject Controls;
    public GameObject cubeFollow;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        skeletonAnimation = other.GetComponent<SkeletonAnimation>();
        skeletonAnimation.AnimationState.SetAnimation(0, hoverboard, true);
        cubeFollow.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
        Controls.GetComponent<BasicPlatformerController>().enabled = false;
     
    }
     private void OnTriggerExit(Collider other)
    {
        cubeFollow.GetComponent<Renderer>().material.SetColor("_Color", Color.white);
       skeletonAnimation.AnimationState.SetAnimation(0, idle, true);
       Controls.GetComponent<BasicPlatformerController>().enabled = true;
    }

    private void OnTriggerStay(Collider other)
    {
        skeletonAnimation = other.GetComponent<SkeletonAnimation>();
        skeletonAnimation.AnimationState.SetAnimation(0, hoverboard, false);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
}